\section{Evaluation}
\label{sec:evaluation}

This section describes the evaluation process used to answer the
research questions posed by this project. In particular, I investigated
the following questions:

\begin{description}[font=\normalfont\emph, nosep]
\item[RQ1: Impact.] What are the energy impacts of indistinguishable
  color changes?
\item[RQ2: Suitability.] Are users able to detect differences between
  the original and optimized \acp{ui}?
\end{description}

\subsection{RQ1: Impact}
\label {sec:evaluation:impact}

\begin{figure}
 \includegraphics{figures/savings.pdf}
   \label{fig:savings}
    \caption{Percentage decrease in energy usage when switching from
    original to optimized executions.}
\end{figure}



To answer the first research question, I compared the energy
consumption of the original executions against a version optimized
with indistinguishable changes and a version optimized by
\citeauthor{Wan:15}'s approach for each considered display.
\Cref{fig:savings} shows the results of this comparison. In the
figure, the top bar chart shows the results for indistinguishable
changes and the bottom bar chart shows the results for
\citeauthor{Wan:15}'s approach.  Within each bar chart, the y-axis
shows the percentage decrease in energy usage of the optimized version
over the original version and the x-axis shows the considered
executions.  For each execution, there is a group of three bars, one
for each considered display.  In each group, the top bar corresponds
to the \ac{s2} display, the middle bar corresponds to the \ac{amoled}
display, and the bottom bar corresponds to the \ac{nexus} display.

As \cref{fig:savings} shows, the largest savings for indistinguishable
changes are obtained on the \ac{amoled} display where the percentage
decrease in energy usage ranges from \SIrange{3.3}{7.8}{\percent} with
a mean of \SI{7.1}{\percent}, and a median of \SI{7.51}{\percent}.
The savings are more modest on the \ac{s2} display with the decrease
ranging from \SIrange{0.9}{4.9}{\percent} with a mean of
\SI{4.1}{\percent}, and a median of \SI{4.35}{\percent}.  Finally,
savings on the \ac{nexus} display are essentially nonexistent with a
maximum decrease of \SI{0.001}{\percent}.  The savings for
\citeauthor{Wan:15}'s approach follow a similar trend, albiet with
larger decreases, including negligible savings on the \ac{nexus}
display. The variance in savings across displays can be explained by
the slopes of the response curves between power consumption and RGB
channel intensities.  For the \ac{amoled} display, the slopes of these
curves are relatively steep.  This means that even a small change in a
channel's intensity can result in a significant power reduction.
Conversely, the slopes of the curves for the \ac{nexus} display are
nearly flat.  This means that even large intensity changes have
virtually no impact on power consumption.

The difference in performance among the two types of changes is
unsurprising, as the priorities of the approaches are different.
Indistinguishable changes focus on keeping the aesthetics of
application unchanged, while \citeauthor{Wan:15}'s approach focuses on
achieving the maximum possible energy savings.  While their results
show the potential of \ac{ui} optimization, I believe
indistinguishable changes are more realistic as they are more
acceptable for developers (see~\cref{fig:faceq}).

Overall, I believe that these results are encouraging for developers.
Even when restricted to changes that maintain the application's
aesthetics, changing the colors of the \ac{ui} can result in moderate
energy savings.

\subsection{RQ2: Suitability}
\label{sec:evaluation:suitability}

To investigate the second research question, I conducted an online
survey to determine whether the color changes made to the screenshots
are noticeable to typical application users.  As the subjects of the
study, I arbitrarily selected \num{40} screenshots, \numrange{2}{3}
representative images for each app, always including the app's initial
page.  As survey respondents, I recruited three University of
Delaware graduate students. The respondents were not involved in the
research and were not compensated for their participation.

During the survey, each respondent was asked to answer \num{50}
questions.  Each question showed the respondent a pair of screenshots,
side-by-side, consisting of the original and optimized versions
(\eg~\cref{fig:faceq:original,fig:faceq:optimized}) and asked the
respondent to rate their agreement, on a 4-level Likert scale of
Strongly Agree, Agree, Disagree, or Strongly Disagree, with the
following statement: \enquote{The colors in these images are
  effectively the same}.  In order to avoid potential learning
effects, both the order in which the questions were asked and the
order of the original and optimized screenshots were randomized.  In
addition, to check the quality of the responses, \num{10} of the
questions used the same image for both the original and optimized
version.  Questions with identical screenshots are not considered in
the remainder of this evaluation.

\begin{figure}
  \centering
  \includegraphics[scale=0.7]{figures/survey.pdf}
  \caption{Bar graph showing the answers to the survey.}
  \label{fig:survey}
\end{figure}

\Cref{fig:survey} shows a summary of the responses for the \num{120}
questions I considered.  The x-axis shows the number of responses
while the color of the bar indicates the rating type.  As the figure
shows: Strongly Disagree was chosen \num{14} times, Disagree was
chosen \num{28} times, Agree was chosen \num{73} times, and Strongly
Agree was chosen \num{5} times.

Again, I believe that these results are encouraging for developers.
Even in a situation where the respondents were primed to look for
differences, were able to view both images side-by-side, and had an
unlimited amount of time to make a decision, the majority of
respondents (\SI{65}{\percent}) Strongly Agreed or Agreed that the
colors in the images were effectively the same.  Moreover, all of
the Strongly Disagree and the majority of the Disagree ratings were
given when most of the image consisted of a light-color background
(\eg~\cref{fig:faceq}), situations where the changes are most
noticeable.


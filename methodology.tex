\section{Empirical Study}
\label{sec:methodology}

This section describes the details of the study design, including the
considered \ac{oled} displays and executions, alternate color
generation approach, and data collection protocol.

\subsection{Considered \ac{oled} Displays}
\label{sec:methodology:displays}

The experiments were conducted using three different \ac{oled}
displays: \ac{amoled}, \ac{s2}, and \ac{nexus}.  Because \ac{oled}
displays vary in their power consumption profiles and there are a
variety of \ac{oled} displays in use, considering multiple displays
helps address the threat of generalizability.  We chose these specific
displays because they were used in prior color-optimization work by
\citeauthor{Wan:15}~\cite{Wan:15}.  As such, I can compare the energy
impacts of indistinguishable changes against their approach.

Each considered display has an associated power model that can be used
to calculate the power needed to display a color. I use these models
both to find the set of optimized, alternate colors for each display
(see~\cref{sec:methodology:optimize}) and to calculate the energy
consumption of an execution (see~\cref{sec:methodology:procedure}).
These models were built by \citeauthor{Wan:15} following the process
described by \citeauthor{Dong:12}l~\cite{Dong:12}.

\begin{figure}[t]
  \centering
  \begin{subfigure}[b]{.3\columnwidth}
    \centering
    \includegraphics[width=.95\textwidth]{figures/faceq-original}
    \subcaption{}
    \label{fig:faceq:original}
  \end{subfigure}%
  \begin{subfigure}[b]{.3\columnwidth}
    \centering
    \includegraphics[width=.95\textwidth]{figures/faceq-optimized}
    \subcaption{}
    \label{fig:faceq:optimized}
  \end{subfigure}%
  \begin{subfigure}[b]{.3\columnwidth}
    \centering
    \includegraphics[width=.95\textwidth]{figures/faceq-mian}
    \subcaption{}
    \label{fig:faceq:mian}
  \end{subfigure}
  \caption{Original screenshot (\subref{fig:faceq:original}) and
    versions optimized using indistinguishable changes
    (\subref{fig:faceq:optimized}) and \citeauthor{Wan:15}'s approach
    (\subref{fig:faceq:mian}).}
  \label{fig:faceq}
\end{figure}

\subsection{Considered Application Executions}
\label{sec:methodology:executions}

\begin{table}[t]
\caption{Considered executions.}
\centering
\begin{tabu} { 
    l
    S[table-format=3] 
    S[table-format=3] 
  } 
  \toprule
  \multicolumn{1}{c}{Application} & {Duration (s)} & {\# Screenshots} \\
  \midrule
  Facebook                 & 554        & 116              \\
  Facebook Messenger       & 268        & 55               \\
  FaceQ                    & 470        & 96               \\
  Flashlight               & 51         & 20               \\
  Instagram                & 429        & 93               \\
  Pandora                  & 278        & 75               \\
  Skype                    & 254        & 35               \\
  Snapchat                 & 465        & 142              \\
  Twitter                  & 388        & 101              \\
  WhatsApp                 & 242        & 65               \\
  \bottomrule
\end{tabu}
\label{tab:executions}
\end{table}

\Cref{tab:executions} shows the \num{10} executions I
considered. Because I am only interested in \ac{ui} changes, I do
not need to collect detailed traces of the internals of the
executions.  Instead, I can capture an execution as a list of tuples
\(\langle screenshot, duration \rangle\) where \(screenshot\) is a
copy of the image displayed on the screen and \(duration\) is the
length of time the screenshot was visible.  In \cref{tab:executions}
the first column, \emph{Application}, shows the name of the
application used to generate the execution; the second column,
\emph{Duration} shows the total length of the execution in seconds
(\ie the sum of the durations of the tuples in the execution); and the
final column, \emph{\# Screenshots} shows the total number of captured
screenshots.

For the considered displays
(see~\cref{sec:methodology:displays}), I chose these executions
because they are used in prior work (\ie~\cite{Wan:15}) which allows
for a direct comparison of the results.  Originally, the applications
for each execution where chosen because they were the top 10
applications in the Google Play Store as of August 2014.  To collect
the executions, \citeauthor{Wan:15} manually interacted with each
application, conducting a typical user scenario.  While the
application was executing, a screenshot and associated timestamp was
recorded each time the \ac{ui} changed.  This information was post-processed to identify the length of time each screenshot was visible.

\subsection{Optimized Execution Creation}
\label{sec:methodology:optimize}

\begin{algorithm}[t]
  \small
  \caption{Execution Optimization Procedure.}
  \label{alg:optimize}
  \begin{algorithmic}[1]
    \renewcommand{\algorithmicrequire}{\textbf{Input:}}
    \renewcommand{\algorithmicensure}{\textbf{Output:}}
    
    \Require $execution$: execution to optimized
    \Require $display$: display to optimize for
    \Ensure optimized execution
    
    \Function{OptimizeExecution}{execution, display} 
    \State newExecution $\gets \lbrack \rbrack$ \label{alg:optimize-execution:newExecution}
    \ForAll{$\langle \text{image}, \text{duration} \rangle \in$ execution} \label{alg:optimize-execution:loop}
    \State width $\gets$ \Call{widthOf}{image} \label{alg:optimize-execution:width}
    \State height $\gets$ \Call{heightOf}{image} \label{alg:optimize-execution:height}
    \State newImage $\gets$ \Call{newImage}{width, height} \label{alg:optimize-execution:newImage}
    \ForAll{w $\in 0 \ldots$ width} \label{alg:optimize-execution:forWidth}
    \ForAll{h $\in 0 \ldots$ height} \label{alg:optimize-execution:forHeight}
    \State color = \Call{getColor}{w, h, image} \label{alg:optimize-execution:getColor}
    \State alt = \Call{OptimizeColor}{color, display} \label{alg:optimize-execution:optimizeColor}
    \State \Call{setColor}{w, h, newImage, alt} \label{alg:optimize-execution:setColor}
    \EndFor
    \EndFor
    \State \Call{append}{newExecution, $\langle \text{newImage}, \text{duration} \rangle$} \label{alg:optimize-execution:append}
    \EndFor
    \State \Return newExecution \label{alg:optimize-execution:return}
    \EndFunction
  \end{algorithmic}%
  \begin{algorithmic}[1]
    \renewcommand{\algorithmicrequire}{\textbf{Input:}}
    \renewcommand{\algorithmicensure}{\textbf{Output:}}
    \Require \(color\): color to optimize     
    \Require \(display\): display to optimize for
    \Ensure alternate color that
    \begin{enumerate*} 
    \item is visually indistinguishable from \(color\)
    \item consumes the least power when displayed on \(display\)
    \end{enumerate*}
    
    \Function{OptimizeColor}{color, display}
    \State cache \(\gets\) \Call{cacheFor}{display} \label{alg:optimize-color:cacheFor}
    \If{\Call{!contains}{cache, color}}  \label{alg:optimize-color:contains}
    \State alternate \(\gets\) \Call{neighborsOf}{color} \label{alg:optimize-color:neighborsOf}
    \State \qquad \textbf{filter} \{ alt \(\Rightarrow\) \Call{distance}{color, alt} \(< 2.3 \) \} \label{alg:optimize-color:filter}
    \State \qquad \textbf{minBy} \{ alt \(\Rightarrow\) \Call{power}{display, alt} \} \label{alg:optimize-color:minBy}
    \State \Call{put}{cache, color, alternate} \label{alg:optimize-color:put}
    \EndIf
    \State \Return \Call{get}{cache, color} \label{alg:optimize-color:return}
    \EndFunction
  \end{algorithmic}
\end{algorithm}

To generate the optimized versions of each execution, I used the
procedures shown in \cref{alg:optimize}.  As input, the procedure
takes \(execution\)---the execution to optimize---and
\(display\)---the display to optimize for---and produces an optimized
version of the execution as output.  To hold the optimized execution,
an empty list is initialized
(\cref{alg:optimize-execution:newExecution}).  Then each screenshot in
the original execution is optimized
(\crefrange{alg:optimize-execution:loop}{alg:optimize-execution:append}).
The first step in this process is to determine the width and height of
the screenshot
(\cref{alg:optimize-execution:width,alg:optimize-execution:height})
and to create a new image of the appropriate dimensions
(\cref{alg:optimize-execution:newImage}).  Then the dimensions are
iterated over
(\cref{alg:optimize-execution:forWidth,alg:optimize-execution:forHeight})
in order to find the color of each pixel in the original image
(\cref{alg:optimize-execution:getColor}).  The alternate color is
obtained by invoking the OptimizeColor function
(\cref{alg:optimize-execution:optimizeColor}) and set as the color of
the corresponding pixel in the new image
(\cref{alg:optimize-execution:setColor}).  Finally, a tuple consisting
of the new image and the original duration is added to the new
execution (\cref{alg:optimize-execution:append}).  After all
screenshots contained in the original execution are optimized, the
completed optimized execution is returned
(\cref{alg:optimize-execution:return}).

The OptimizeColor function takes as input \(color\)---the color to be
optimized---and \(display\)---the \ac{oled} display to optimize
for---and produces as output an alternate color that
\begin{enumerate*} 
\item is visually indistinguishable from \(color\)
\item consumes the least power when displayed on \(display\).
\end{enumerate*}
%
Because the alternate color generation procedure is deterministic with
respect to its inputs, caching prevents unnecessarily recomputing
results (\cref{alg:optimize-color:cacheFor}).  Using a cache for each
display is necessary because they each have a different power model.
Using the cache, the procedure checks whether the alternate color for
the given color has been computed
(\cref{alg:optimize-color:contains}).  If so, the alternate color is
returned (\cref{alg:optimize-color:return}).  If not, the alternate
color is computed
(\crefrange{alg:optimize-color:neighborsOf}{alg:optimize-color:put}).
Because searching the entire color space would be prohibitively
expensive, only colors within a predefined neighborhood are considered
(\cref{alg:optimize-color:neighborsOf}).  Here, the color's
neighborhood is the set of colors whose RGB components are within
\num{30} of the original color.  The set of neighbor colors is then
filtered by calculating the distance between each neighbor and the
original color using the CIEDE2000 color difference
formula~\cite{Sharma:05} (\cref{alg:optimize-color:filter}). This
formula takes into account various properties of human visual
perception and calculates a difference value between two colors.
Neighbors with a CIEDE2000 difference of less then \num{2.3} are
retained as they are generally considered to be identical to the human
eye~\cite{Sharma:05}.  The most power-efficient of the remaining
neighbors is then selected by identifying the one with the minimum
power consumption for the given display
(\cref{alg:optimize-color:minBy}).  Here, power consumption is
calculated using the power models described in
\cref{sec:methodology:displays}.  Finally, the selected alternate is
stored in the cache (\cref{alg:optimize-color:put}) and returned
(\cref{alg:optimize-color:return}).

As an example of the results of this optimization process, consider
the images shown in \cref{fig:faceq}.  This figure shows the original
(\subref{fig:faceq:original}) and optimized
(\subref{fig:faceq:optimized}) versions of one screenshot from the
execution of the FaceQ application.  While close, side-by-side
inspection of the figures can reveal some differences (\eg the
background is slightly darker), the overall application aesthetics
remain unchanged, especially when compared to the version optimized by
\citeauthor{Wan:15}'s approach (\subref{fig:faceq:mian}).

\subsection{Energy Consumption Calculation}
\label{sec:methodology:procedure}

To calculate the energy consumption of an execution, I again use the
power models for each display (see~\cref{sec:methodology:displays}).
For each tuple in an execution, I first calculate the total power
needed to display the screenshot by adding the power consumption of
each pixel's color.  To convert power consumption to energy usage, I
multiply by the screenshot's duration.  Finally, I sum the energy
usage of each screenshot to obtain the energy usage for the execution.

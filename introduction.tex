\section{Introduction}
\label{sec:introduction}

In 2017, mobile app downloads are expected to reach 268.69
billion~\cite{Statista:17}. People spend countless hours every day
using their apps. But, while phones are becoming more powerful, they
still have a very big limitation: battery life. The most popular apps
usually make use of the camera and GPS, and show pictures and videos.
These features are very costly for the battery, leading to unpleasant
downtimes when your phone is simply dead. Thus, it is crucial to
find a way to extend battery life.

Because displays are one of the biggest consumers of battery power
\cite{Li:14}, researchers have investigated approaches for reducing
screen energy usage. One promising direction, applicable when
\ac{oled} screens are used, is to change the colors of an app's
\ac{ui} (\eg~\cite{Wan:15, Linares:15, Dong:09m, Li:15}). Because
displaying darkers colors tends to consume less power, switching light
colors (\eg white) to dark colors (\eg black) can result in
significant savings. Unfortunately, even when care is taken to ensure
that visual properties of the \ac{ui} are maintained (\eg contrast
ratios between text and background elements), such changes often
compromise the aesthetics of the application~\cite{Linares:15}. I
believe developers would prefer not to sacrifice the look of their app
as it is a much more important factor for achieving popularity and
downloads than battery expenditure.

In this project, I investigate whether it is possible to significantly
reduce screen energy consumption by making only
\emph{indistinguishable} color changes to the \ac{ui}. While
constraining the search space to only indistinguishable changes may
result in smaller energy reductions, it has the benefit of maintaining
application aesthetics which removes a major barrier to applying the
suggested changes. To identify indistinguishable color changes, I
first determine the set of colors that are visually indistinguishable
from the original color using the CIEDE2000 color difference
formula~\cite{Sharma:05}. Then I select the color from this
set with the lowest power consumption for the target \ac{oled}
display.  Given the set of alternate colors, I can compute the energy
savings for an execution if the original colors were replaced with
their alternates.

To investigate the impacts of indistinguishable color changes, I
conducted an empirical study of 10 Android applications. In
particular, I am interested in answering two questions:
\begin{enumerate*}
\item What are the energy impacts of indistinguishable color changes?
\item Are users able to detect differences between the original and
  optimized \acp{ui}?
\end{enumerate*}
%
The results of this evaluation are promising as they show moderate
energy saving are achieved, without unduly affecting the \acp{ui}. The 
algorithm achieved a maximum of \SI{7.8}{\percent} energy saving. Furthermore,
I conducted a survey to evaluate if users are able to
distinguish between the original and optimized \acp{ui}. Subjects were
presented with the statement \enquote{The colors in these images are
  effectively the same}, referring to pairs of original and optimized
images. \SI{65}{\percent} of participants agreed with the statement.
These results show that if the algorithm is implemented, developers
will be able to offer a more energy conscious version of their
application, without sacrificing its appearance.
